<?php

namespace Brandbassador\Plugin\Observer;

use Brandbassador\Plugin\Helper\Data;
use Magento\Framework\Event\ObserverInterface;
use Magento\Store\Model\StoreManager;
use Magento\Framework\HTTP\Client\Curl;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Message\ManagerInterface as MessageManager;

class AuthKeyChange implements ObserverInterface
{
    public function __construct(StoreManager $storeManager, Data $helper, Curl $curl, MessageManager $messageManager) 
    {
        $this->storeManager = $storeManager;
        $this->helper = $helper;
        $this->curl = $curl;
        $this->messageManager = $messageManager;
    }

    public function execute(EventObserver $observer)
    {   
        $apiUrl = $this->helper->getBrandbassadorApiUrl();

        // retrieve auth key and api base url
        // and send these information to BB system
        $this->curl->addHeader('Cache-Control', 'no-cache');
        $this->curl->addHeader('Content-Type', 'application/x-www-form-urlencoded');
        $this->curl->post($apiUrl . '/admin/brands/registerWebshopPlugin', [
            'authKey' => $this->helper->getGeneralConfig('auth_key'),
            'api' => $this->storeManager->getStore()->getBaseUrl() . 'brandbassador',
        ]);

        $response = $this->curl->getBody();

        // If error message has been provided
        // display an error notification
        // and delete auth key from config
        if (json_decode($response)) {
            $response = json_decode($response);

            if (isset($response->statusCode) && $response->statusCode >= 400) {
                $this->messageManager->addWarning("Your already linked your webshop to Brandbassador.");

                return $this;
            }
        }
        
        // If everything is fine
        // save tracking pixel key to config
        $this->helper->setGeneralConfig('tracking_pixel_key', $response);

        return $this;
    }
}