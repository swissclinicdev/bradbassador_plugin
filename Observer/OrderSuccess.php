<?php

namespace Brandbassador\Plugin\Observer;

use Magento\Framework\Event\ObserverInterface;
use Brandbassador\Plugin\Helper\Data;
use Magento\Sales\Model\Order;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Model\Session as CustomerSession;

class OrderSuccess implements ObserverInterface
{
    public function __construct(CheckoutSession $checkoutSession, CustomerSession $customerSession, Order $order, Data $helper) 
    {
        $this->checkoutSession = $checkoutSession;
        $this->customerSession = $customerSession;
        $this->helper = $helper;
        $this->order = $order;
    }

    public function execute(EventObserver $observer)
    {   
        $order = $this->order->load($this->checkoutSession->getLastOrderId());
        $key = $this->helper->getGeneralConfig('tracking_pixel_key');
        $ref = $this->customerSession->getData('bbref', true);

        // Check if current order contains a discount code or reference link
        if ($order->getCouponCode() || $ref) {
            $total = floatval($order->getGrandTotal()) - floatval($order->getShippingAmount()) - floatval($order->getTaxAmount());
            $total = number_format($total, 2);

            // Build params required by the tracking pixel request
            $params = [
            	'order_id' => $order->getIncrementId(),
            	'total' => $total,
            	'currency' => $order->getOrderCurrencyCode(),
            	'key' => $key,
            ];

            // If a discount code is provided send it to BB system
            if ($order->getCouponCode()) {
                $params['code'] = $order->getCouponCode();
            }
            // If discount code is not provided
            // check if there is a reference link set
            else if ($ref) {
            	$params['tracking_link'] = 'true';
            	$params['ref'] = $ref;
            }

            // Silently place the request to the tracking pixel
            // so possible errors don't interfere in the buying process
            $apiUrl = $this->helper->getBrandbassadorApiUrl();
            @file_get_contents($apiUrl . '/tracking/pixel.gif?' . http_build_query($params));
        }

        return $this;
    }
}