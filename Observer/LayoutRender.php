<?php

namespace Brandbassador\Plugin\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Customer\Model\Session as CustomerSession;

class LayoutRender implements ObserverInterface
{
    public function __construct(Http $request, CustomerSession $customerSession) 
    {
        $this->request = $request;
        $this->customerSession = $customerSession;
    }

    public function execute(EventObserver $observer)
    {   
        $ref = $this->request->getParam('ref');

        if ($ref) {
            $this->customerSession->setData('bbref', $ref);
        }

        return $this;
    }
}