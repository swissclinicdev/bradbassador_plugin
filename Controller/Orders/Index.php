<?php
 
namespace Brandbassador\Plugin\Controller\Orders;
 
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Action\Action;
use Brandbassador\Plugin\Helper\Data;
use Brandbassador\Plugin\Model\Order;
 
class Index extends Action
{
    public function __construct(Context $context, Data $helper, Order $order)
    {
        parent::__construct($context);
        $this->helper = $helper;
        $this->order = $order;
    }
 
    public function execute()
    {
        $request = $this->getRequest()->getParams();
        $response = $this->resultFactory->create(ResultFactory::TYPE_JSON);

        // check if auth key is valid
        if (!$this->helper->validateAuthKey($request)) {
            $response->setData([
                'status' => 'error',
                'details' => 'invalid_auth_key',
            ]);
            return $response;
        }

        // filter by given codes
        if (isset($request['codes'])) {
        	$codes = explode(',', $request['codes']);
        	$this->order->addFilter('coupon_code', ['in' => $codes]);
        }

        // filter by start date
        if (isset($request['start_date'])) {
        	$this->order->addFilter('created_at', ['gteq' => $request['start_date']]);
        }

        // filter by end date
        if (isset($request['end_date'])) {
        	$this->order->addFilter('created_at', ['lteq' => $request['end_date']]);
        }

        $response->setData([
        	'orders' => $this->order->get(),
        ]);
        return $response;
    }
}