<?php
 
namespace Brandbassador\Plugin\Controller\Index;
 
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Action\Action;
use Brandbassador\Plugin\Helper\Data;

class Index extends Action
{
    public function __construct(Context $context, Data $helper)
    {
        parent::__construct($context);
        $this->helper = $helper;
    }
 
    public function execute()
    {
        $response = $this->resultFactory->create(ResultFactory::TYPE_JSON);

        $response->setData([
            'name' => $this->helper->getName(),
            'version' => $this->helper->getVersion(),
        ]);

        return $response;
    }
}