<?php
 
namespace Brandbassador\Plugin\Controller\Coupons;
 
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Action\Action;
use Brandbassador\Plugin\Helper\Data;
use Brandbassador\Plugin\Model\Coupon;

 
class Index extends Action
{
    public function __construct(Context $context, Data $helper, Coupon $coupon)
    {
        parent::__construct($context);
        $this->helper = $helper;
        $this->coupon = $coupon;
    }
 
    public function execute()
    {
        $request = $this->getRequest()->getParams();
        $response = $this->resultFactory->create(ResultFactory::TYPE_JSON);

        // check if auth key is valid
        if (!$this->helper->validateAuthKey($request)) {
            $response->setData([
                'status' => 'error',
                'details' => 'invalid_auth_key',
            ]);
            return $response;
        }        

        // try adding coupon to the shop if request is valid
        if ($this->coupon->validate($request)) {
            $this->coupon->create();
        } 

        // send status success
        // or details about the generated errors
        if ($this->coupon->hasErrors()) {
            $response->setHttpResponseCode(400);
            $response->setData([
                'status' => 'error',
                'details' => $this->coupon->getErrors(),
            ]);
        } else {
            $response->setData(['status' => 'success']);
        }

        return $response;
    }
}