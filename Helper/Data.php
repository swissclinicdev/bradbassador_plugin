<?php

namespace Brandbassador\Plugin\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Module\ModuleList;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManager;
use Magento\Config\Model\ResourceModel\Config;
use Magento\Framework\HTTP\Client\Curl;
use Magento\SalesRule\Model\ResourceModel\Rule\Collection as SalesRules;
use Magento\Customer\Model\ResourceModel\Group\Collection as CustomerGroups;

class Data extends AbstractHelper
{
    const XML_PATH_HELLOWORLD = 'brandbassador/';
    const PLUGIN_NAME = 'Brandbassador_Plugin';

    const DEVELOPMENT = 'development';
    const STAGING = 'staging';
    const PRODUCTION = 'production';

    public function __construct(Context $context, ModuleList $moduleList, StoreManager $storeManager, SalesRules $salesRules, CustomerGroups $customerGroups, Curl $curl, Config $config) 
    {
        parent::__construct($context);

        $this->moduleList = $moduleList;
        $this->storeManager = $storeManager;
        $this->salesRules = $salesRules;
        $this->customerGroups = $customerGroups;
        $this->curl = $curl;
        $this->config = $config;
    }

    /**
     * Retrieve specific config value
     */
    public function getConfigValue($field, $storeId = null)
    {
        return $this->scopeConfig->getValue(
            $field, ScopeInterface::SCOPE_STORE, $storeId
        );
    }

    /**
     * Set specific config value
     */
    public function setConfigValue($field, $value)
    {
        return $this->config->saveConfig($field, $value, 'default', 0);
    }

    /**
     * Retrieve general config value
     * specific to brandbassador plugin
     */
    public function getGeneralConfig($field, $storeId = null)
    {
        return $this->getConfigValue(self::XML_PATH_HELLOWORLD . 'general/' . $field, $storeId);
    }

    /**
     * Set general config value
     * specific to brandbassador plugin
     */
    public function setGeneralConfig($field, $value)
    {
        return $this->setConfigValue(self::XML_PATH_HELLOWORLD . 'general/' . $field, $value);
    }

    /**
     * Retrieve environment configuration
     */
    public function getEnvironment()
    {
        $env = $this->getConfigValue('environment');

        if ($env == self::DEVELOPMENT || $env == self::STAGING) {
            return $env;
        }

        return self::PRODUCTION;
    }

    /**
     * Retrieve base API Url for Brandbassador Platform.
     */
    public function getBrandbassadorApiUrl() {
        $apiUrl = 'https://api.brandbassador.com';

        if ($this->getEnvironment() == self::DEVELOPMENT) {
            $apiUrl = 'https://dev.brandbassador.com';
        }
        else if ($this->getEnvironment() == self::STAGING) {
            $apiUrl = 'https://staging.brandbassador.com';
        }

        return $apiUrl;
    }

    /**
     * Convert the given amount from USD to webshop base currency
     */
    public function convertToBaseCurrency($amount) 
    {
        $apiUrl = $this->getBrandbassadorApiUrl();

        // use Brandbassador API for conversion
        $this->curl->addHeader('Cache-Control', 'no-cache');
        $this->curl->addHeader('Content-Type', 'application/x-www-form-urlencoded');
        $this->curl->post($apiUrl . '/brands/convertCurrency', [
            'from' => 'USD',
            'to' => $this->getBaseCurrencyCode(),
            'amount' => $amount
        ]);

        $response = @json_decode($this->curl->getBody());

        // Check if everything worked fine
        // and return empty in case of exception
        if ($this->curl->getStatus() == 200 && $response && isset($response->amount)) {
            return floatval($response->amount);
        }

        return;
    }

    /**
     * Check if an auth key is provided
     * and it is the same key as the local one
     * 
     * @param  array
     * @return bool
     */
    public function validateAuthKey($request) 
    {
        if (isset($request['auth_key']) && $request['auth_key'] == $this->getGeneralConfig('auth_key')) {
            return true;
        }

        return false;
    }

    /**
     * Get Package version from config file
     */
    public function getName()
    {   
        return self::PLUGIN_NAME;
    }

    /**
     * Get Package version from config file
     */
    public function getVersion()
    {   
        return $this->moduleList->getOne($this->getName())['setup_version'];
    }

    /**
     * Retrieve shop base currency code
     */
    public function getBaseCurrencyCode()
    {
        return $this->storeManager->getStore()->getBaseCurrency()->getCode();
    }

    /**
     * Retrieve sales rule by code
     */
    public function getSalesRule($code)
    {
        return $this->salesRules->addFieldToFilter('code',['eq' => $code])->getFirstItem();
    }

    /**
     * Retrieve all available customer groups
     */
    public function getAllCustomerGroupsIds()
    {
        $this->customerGroups->addFieldToFilter('customer_group_code', array('nlike'=>'%auto%'));

        $ids = [];
        foreach ($this->customerGroups as $group){
            $ids[] = $group->getId();
        }

        return $ids;
    }

    /**
     * Retrieve all available websites
     */
    public function getAllWebsitesIds()
    {
        $websites = $this->storeManager->getWebsites();

        $ids = [];
        foreach ($websites as $website){
            $ids[] = $website->getId();
        }

        return $ids;
    }
}