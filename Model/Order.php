<?php
 
namespace Brandbassador\Plugin\Model;

use Magento\Sales\Model\ResourceModel\Order\Collection as Orders;


class Order 
{
	public function __construct(Orders $orders)
	{
	    $this->orders = $orders;
	}

    public function addFilter($filter, $value) 
    {
        $this->orders->addFieldToFilter($filter, $value);
    }

    public function get()
    {
        $orders = [];
        foreach ($this->orders as $order) {
            $orders[] = $order->getData();
        }

        return $orders;
    }
}